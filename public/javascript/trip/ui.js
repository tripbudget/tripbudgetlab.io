export const ui = {
  edit: document.getElementById('trip-edit'),
  share: document.getElementById('trip-share'),
  importModal: document.querySelector('[data-modal=trip-import]'),
  importFile: document.getElementById('trip-import-file'),
  form: document.getElementById('trip-form'),
  period: document.getElementById('trip-period'),
  budget: document.getElementById('trip-budget'),
  startAt: document.getElementById('trip-start-at'),
  endAt: document.getElementById('trip-end-at'),
  includeStartAt: document.getElementById('trip-start-at-including'),
  includeEndAt: document.getElementById('trip-end-at-including')
}
