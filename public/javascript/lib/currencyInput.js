export const start = () => {
  document.querySelectorAll('input[data-currency]').forEach((input) => {
    input.addEventListener('invalid', () =>
      input.setCustomValidity('Value must contains only numbers and periods: 0.00')
    )
  })
}
