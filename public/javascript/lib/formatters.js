export const currencyFormatter = (value) =>
  (value || 0).toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
    roundingMode: 'floor',
    minimumFractionDigits: 2
  })

export const dateFormatter = (value, utc = false) => {
  const date = (value instanceof Date) ? value : new Date(value)
  let parts

  if (utc) {
    parts = [
      date.getUTCFullYear(),
      (date.getUTCMonth() + 1).toString().padStart(2, '0'),
      date.getUTCDate().toString().padStart(2, '0')
    ]
  } else {
    parts = [
      date.getFullYear(),
      (date.getMonth() + 1).toString().padStart(2, '0'),
      date.getDate().toString().padStart(2, '0')
    ]
  }

  return parts.join('-')
}

export const timeFormatter = (value) => {
  const date = (value instanceof Date) ? value : new Date(value)

  return [
    date.getHours().toString().padStart(2, '0'),
    date.getMinutes().toString().padStart(2, '0'),
  ].join(':')
}

export const datetimeFormatter = (value) => {
  return [
    dateFormatter(value),
    timeFormatter(value)
  ].join('T')
}

export const percentFormatter = (value) => {
  const number = Number(value)

  if (number <= 0) return '0%'
  if (number >= 1) return '100%'

  return `${Math.floor(number * 100)}%`
}

export const ensureFloat = function(value) {
  if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value)) {
    return Number(value)
  }

  return NaN
}

export const fixFloat = (value, decimalSize = 2) => {
  const p = Math.pow(10, decimalSize)

  return Math.trunc(Number(value) * p) / p
}
