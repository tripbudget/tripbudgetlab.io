import { ui } from './ui.js'
import {
  currencyFormatter,
  percentFormatter
} from '../lib/formatters.js'

export const load = (trip) =>
  update(trip)

export const update = (trip) => {
  updateGraph(ui.days, trip.days)
  updateGraph(ui.budget, trip.budget, currencyFormatter)
  updateGraph(ui.today, trip.today, currencyFormatter)

  Object.entries(ui.daily).forEach(([type, element]) =>
    element.textContent = currencyFormatter(trip.daily[type])
  )

  Object.entries(ui.categories).forEach(([category, element]) =>
    element.textContent = currencyFormatter(trip.categories[category])
  )

  return trip
}

const updateGraph = (nodes, { total, resting, used }, formatter = null) => {
  nodes.total.innerHTML = formatter ? formatter(total) : total
  nodes.resting.innerHTML = formatter ? formatter(resting) : resting
  nodes.used.innerHTML = formatter ? formatter(used) : used

  if ((used || 0) == 0 || (total || 0) == 0) {
    nodes.bar.used.style.width = percentFormatter(0)
    nodes.bar.used.querySelector('p').innerHTML = ''
  } else {
    const percent = percentFormatter(used / total)

    nodes.bar.used.style.width = percent
    nodes.bar.used.querySelector('p').innerHTML = percent
  }
}
