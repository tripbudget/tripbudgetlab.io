const TYPES = ['days', 'budget', 'today']

const graphElements = (type) => {
  const graph = document.querySelector(`[data-graph=${type}]`)

  return {
    total: graph.querySelector('[data-graph-total]'),
    resting: graph.querySelector('[data-graph-resting]'),
    used: graph.querySelector('[data-graph-used]'),
    bar: {
      used: graph.querySelector('[data-graph-bar-used]'),
    }
  }
}

export const ui = TYPES.reduce((result, type) => {
  Object.defineProperty(result, type, {
    get() {
      return graphElements(type)
    }
  })

  return result
}, {
  daily: {
    budget: document.getElementById('dashboard-daily-budget'),
    average: document.getElementById('dashboard-daily-average')
  },
  categories: {
    transport: document.getElementById('dashboard-category-transport'),
    food: document.getElementById('dashboard-category-food'),
    other: document.getElementById('dashboard-category-other'),
  },
})
