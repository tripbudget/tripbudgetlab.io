import * as currencyInput from './lib/currencyInput.js'
import * as modal from './lib/modal.js'

import * as trip from './trip/events.js'
import * as transaction from './transaction/events.js'

import { dispatch } from './reducer.js'

window.addEventListener('load', () => {
  try {
    dispatch('app.start')

    currencyInput.start()
    modal.start()

    trip.startEventListners()
    transaction.startEventListners()
  } catch (error) {
    console.log(error)
  }
})
